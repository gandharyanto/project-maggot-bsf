/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomaggot;

import java.awt.Image;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author ITDEV
 */
public class MyFunctions {

    ImageIcon ii = null;
    boolean isSuccess = false;

    public MyFunctions() {
    }

    public void uploadImage(Callback callback) {
        JFileChooser chooser = new JFileChooser();
        chooser.showOpenDialog(null);
        final File f = chooser.getSelectedFile();
        String filePath = f.getAbsolutePath();
        String filename = f.getName();
        if (f == null) {
            callback.onFailure();
        }
        imageObject.setImagePath(filePath);
        imageObject.setImageName(filename);

        SwingWorker sw = new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                ii = new ImageIcon(ImageIO.read(new File(f.getAbsolutePath())));
                return null;
            }

            @Override
            protected void done() {
                super.done();
                isSuccess = true;
                imageObject.setImageFile(ii);
                callback.onSuccess();
            }
        };
        sw.execute();
    }

    String fname = null;
    byte[] pinsertimage = null;

    public void UploadImageFile(javax.swing.JLabel lblimage, Callback callback) {
        JFileChooser fchoser = new JFileChooser();
        fchoser.showOpenDialog(null);
        File f = fchoser.getSelectedFile();
        fname = f.getAbsolutePath();
        imageObject.setImagePath(fname);
        imageObject.setImageName(f.getName());

        ImageIcon micon = new ImageIcon(fname);
        try {
            File image = new File(fname);
            FileInputStream fis = new FileInputStream(image);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            for (int readnum; (readnum = fis.read(buf)) != -1;) {
                baos.write(buf, 0, readnum);
            }
            pinsertimage = baos.toByteArray();
            imageObject.setImageByte(pinsertimage);
            imageObject.setImageFile(resizeImage(fname, buf, lblimage));
            callback.onSuccess();
        } catch (Exception e) {
            callback.onFailure();
        }
    }

    public void UploadImagesFile(javax.swing.JLabel lblimage, Callback callback) {
        try {
            List<imageObject> imageObjectList = new ArrayList<>();

            JFileChooser jc = new JFileChooser();
            jc.setDialogTitle("Please select pictures");
            jc.setMultiSelectionEnabled(true);
            jc.setFileFilter(new FileNameExtensionFilter(".jpg", "JPG"));
            jc.setFileFilter(new FileNameExtensionFilter(".jpeg", "JPEG"));
            jc.setFileFilter(new FileNameExtensionFilter(".png", "PNG"));

            if (jc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                File[] pictures = jc.getSelectedFiles();
                for (File picture : pictures) {
                    imageObject image = new imageObject();
                    image.setImagePath(picture.getAbsolutePath());
                    image.setImageName(picture.getName());
                    FileInputStream fis = new FileInputStream(picture);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    byte[] buf = new byte[1024];
                    for (int readnum; (readnum = fis.read(buf)) != -1;) {
                        baos.write(buf, 0, readnum);
                    }
                    pinsertimage = baos.toByteArray();
                    image.setImageByte(pinsertimage);
                    image.setImageFile(resizeImage(fname, buf, lblimage));
                    imageObjectList.add(image);

                }

                callback.onSuccessMultipleImage(imageObjectList);
            }
        } catch (Exception e) {
            e.printStackTrace();
            callback.onFailure();
        }
    }

    public ImageIcon resizeImage(String imagePath, byte[] pic, javax.swing.JLabel lblimage) {

        ImageIcon myImage = null;
        if (imagePath != null) {
            myImage = new ImageIcon(imagePath);

        } else {
            myImage = new ImageIcon(pic);
        }

        Image img = myImage.getImage();
        Image img2 = img.getScaledInstance(lblimage.getHeight(), lblimage.getWidth(), Image.SCALE_SMOOTH);
        ImageIcon image = new ImageIcon(img2);
        return image;
    }

}
