/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomaggot;

import javax.swing.ImageIcon;

/**
 *
 * @author ITDEV
 */
public class imageObject {

    private static String imageName;
    private static String imagePath;
    private static ImageIcon imageFile;
    private static byte[] imageByte;

    public static byte[] getImageByte() {
        return imageByte;
    }

    public static void setImageByte(byte[] imageByte) {
        imageObject.imageByte = imageByte;
    }

    public static String getImageName() {
        return imageName;
    }

    public static void setImageName(String imageName) {
        imageObject.imageName = imageName;
    }

    public static String getImagePath() {
        return imagePath;
    }

    public static void setImagePath(String imagePath) {
        imageObject.imagePath = imagePath;
    }

    public static ImageIcon getImageFile() {
        return imageFile;
    }

    public static void setImageFile(ImageIcon imageFile) {
        imageObject.imageFile = imageFile;
    }

}
