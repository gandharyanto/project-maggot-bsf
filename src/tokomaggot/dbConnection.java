/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tokomaggot;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author ITDEV
 */
public class dbConnection {

    public Connection conn;

    public dbConnection() {
    }

    public Connection openConnection() throws ClassNotFoundException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3307/maggotdb?user=root&pass=");
//            System.out.print("Berhasil koneksi\n");
            return conn;
        } catch (Exception e) {
            System.out.println("Error Database " + e.getMessage());
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Tidak ada koneksi yang terbuka.");
            return null;
        }
    }

    public void closeConnection() throws SQLException {
        try {
            if (conn != null) {
                System.out.print("Tutup Koneksi\n");
            }
        } catch (Exception ex) {
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());

        }
    }

    public ResultSet login(String username, String password) {
        ResultSet rsLogin = null;
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();
            rsLogin = stm.executeQuery("SELECT tb_user.*, tb_user_profile.name, tb_user_profile.role, tb_user_profile.id as pid FROM tb_user LEFT JOIN tb_user_profile ON tb_user.id_profile = tb_user_profile.id WHERE username = '" + username + "' AND password = '" + password + "'");

            rsLogin.next();

            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return rsLogin;
    }

    public ResultSet SelectUserProfile(String profileId) {
        ResultSet rsSelectUserProfile = null;
        ResultSet rsSelectUserImage = null;
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectUserProfile = stm.executeQuery("SELECT tb_user_profile.*, tb_blob_image.id as image_id, tb_blob_image.image_name as image_name, tb_blob_image.image_file as image_file FROM tb_user_profile "
                    + "LEFT JOIN tb_blob_image on tb_user_profile.id = tb_blob_image.id_relation WHERE tb_user_profile.id = '" + profileId + "' "
                    + "ORDER BY tb_user_profile.id DESC LIMIT 1");
            rsSelectUserProfile.next();

            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return rsSelectUserProfile;
    }

    public ResultSet SelectUserProfileSeller(String profileId) {
        ResultSet rsSelectUserProfile = null;
        ResultSet rsSelectUserImage = null;
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectUserProfile = stm.executeQuery("SELECT tb_user_profile.*, tb_user_shop.id as id_shop, tb_user_shop.shop_name, tb_user_shop.shop_category, "
                    + "tb_user_shop.shop_address, tb_user_shop.shop_description, tb_user_shop.shop_type FROM tb_user_profile LEFT JOIN tb_user_shop ON "
                    + "tb_user_shop.id_profile = tb_user_profile.id WHERE tb_user_profile.id = " + profileId + ";");
            rsSelectUserProfile.next();

            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return rsSelectUserProfile;
    }

    public boolean InsertImage(String imageName, String imagePath, byte[] imageFile, String relationId, String relationType) {
        try {

            String q = "INSERT INTO tb_blob_image (image_name, image_path, image_file, id_relation, type_relation) VALUES (?,?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(q);
            pst.setString(1, imageName);
            pst.setString(2, imagePath);
            pst.setBytes(3, imageObject.getImageByte());
            pst.setString(4, relationId);
            pst.setString(5, relationType);

            pst.execute();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            Logger.getLogger(dbConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean UpdateUserProfile(String idProfile, String name, String birthPlace, String birthDate, String address, String gender, String email, String role, String phone, String imageName, String imagePath, byte[] imageFile) {
        int rsUpdateUserProfile = 0;
        boolean isSuccess = false;
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            InsertImage(imageName, imagePath, imageFile, idProfile, "user");

            rsUpdateUserProfile = stm.executeUpdate("UPDATE tb_user_profile SET name = '" + name + "', birth_place = '" + birthPlace + "', birth_date = '" + birthDate + "', address = '" + address
                    + "', gender = '" + gender + "', email = '" + email + "', phone = '" + phone + "' WHERE id = '" + idProfile + "';");

            isSuccess = true;
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return isSuccess;
    }

    public int InsertUserRegister(String name, String birthPlace, String birthDate, String address, String gender, String email, String username,
            String password, String role, String phone, String shopName, String shopCategory, String shopAddress, String shopDesc, String shopType) {
        int id = 0;

        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();
            int rsInsertUserProfile = stm.executeUpdate("INSERT INTO tb_user_profile (name, birth_place, birth_date, address, gender, email, phone, role) VALUES ('" + name + "','" + birthPlace + "','" + birthDate + "','" + address + "','" + gender + "','" + email + "','" + phone + "', '" + role + "')");

            Statement stm1 = conn.createStatement();
            ResultSet rsSelectUserProfile = stm1.executeQuery("SELECT id FROM tb_user_profile ORDER BY id DESC");
            rsSelectUserProfile.next();

            id = rsSelectUserProfile.getInt("id");

            Statement stm2 = conn.createStatement();
            int rsInsertUser = stm2.executeUpdate("INSERT INTO tb_user (id_profile, username, password) VALUES ('" + rsSelectUserProfile.getString("id") + "','" + username + "','" + password + "')");
            if (role.equalsIgnoreCase("penjual")) {
                Statement stm3 = conn.createStatement();
                int rsInsertUserShop = stm3.executeUpdate("INSERT INTO tb_user_shop (id_profile, shop_name, shop_category, shop_address, shop_description, shop_type) VALUES ('" + rsSelectUserProfile.getString("id") + "','" + shopName + "','" + shopCategory + "','" + shopAddress + "','" + shopDesc + "','" + shopType + "')");
            }

            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }

        return id;
    }

    public DefaultTableModel SelectProductByShop(String idShop) {
        ResultSet rsSelectProductByShop = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"id", "price", "name", "category", "exp", "desc",
            "stock", "satuan", "address", "insurance", "merk", "contact", "id_shop"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT * FROM tb_product WHERE id_shop = '" + idShop + "'");

            while (rsSelectProductByShop.next()) {
                Object[] data = {rsSelectProductByShop.getString("id"), rsSelectProductByShop.getString("product_price"),
                    rsSelectProductByShop.getString("product_name"), rsSelectProductByShop.getString("product_category"), rsSelectProductByShop.getString("product_exp"),
                    rsSelectProductByShop.getString("product_desc"), rsSelectProductByShop.getString("product_stock"), rsSelectProductByShop.getString("product_satuan"),
                    rsSelectProductByShop.getString("product_address"), rsSelectProductByShop.getString("product_insurance"), rsSelectProductByShop.getString("product_merk"),
                    rsSelectProductByShop.getString("product_contact"), rsSelectProductByShop.getString("id_shop")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public DefaultTableModel SelectProductByShopAndCategory(String idShop, String category) {
        ResultSet rsSelectProductByShop = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"id", "price", "name", "category", "exp", "desc",
            "stock", "satuan", "address", "insurance", "merk", "contact", "id_shop"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT * FROM tb_product WHERE id_shop = '" + idShop + "' AND product_category = '" + category + "'");

            while (rsSelectProductByShop.next()) {
                Object[] data = {rsSelectProductByShop.getString("id"), rsSelectProductByShop.getString("product_price"),
                    rsSelectProductByShop.getString("product_name"), rsSelectProductByShop.getString("product_category"), rsSelectProductByShop.getString("product_exp"),
                    rsSelectProductByShop.getString("product_desc"), rsSelectProductByShop.getString("product_stock"), rsSelectProductByShop.getString("product_satuan"),
                    rsSelectProductByShop.getString("product_address"), rsSelectProductByShop.getString("product_insurance"), rsSelectProductByShop.getString("product_merk"),
                    rsSelectProductByShop.getString("product_contact"), rsSelectProductByShop.getString("id_shop")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public DefaultTableModel SelectProductByShopAndName(String idShop, String category) {
        ResultSet rsSelectProductByShop = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"id", "price", "name", "category", "exp", "desc",
            "stock", "satuan", "address", "insurance", "merk", "contact", "id_shop"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT * FROM tb_product WHERE id_shop = '" + idShop + "' AND product_name LIKE '%" + category + "%'");

            while (rsSelectProductByShop.next()) {
                Object[] data = {rsSelectProductByShop.getString("id"), rsSelectProductByShop.getString("product_price"),
                    rsSelectProductByShop.getString("product_name"), rsSelectProductByShop.getString("product_category"), rsSelectProductByShop.getString("product_exp"),
                    rsSelectProductByShop.getString("product_desc"), rsSelectProductByShop.getString("product_stock"), rsSelectProductByShop.getString("product_satuan"),
                    rsSelectProductByShop.getString("product_address"), rsSelectProductByShop.getString("product_insurance"), rsSelectProductByShop.getString("product_merk"),
                    rsSelectProductByShop.getString("product_contact"), rsSelectProductByShop.getString("id_shop")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public ResultSet SelectProductById(String id) {
        ResultSet rsSelectProductByShop = null;
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT * FROM tb_product WHERE id = '" + id + "'");

            rsSelectProductByShop.next();

            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return rsSelectProductByShop;
    }

    public DefaultTableModel SelectProductAllToDataTable() {
        ResultSet rsSelectProductByShop = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"Tampilan Product", "Harga (Rp)", "Nama", "Sisa Barang", "Toko"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT tb_product.*, tb_user_shop.shop_name as shop_name FROM tb_product LEFT JOIN tb_user_shop on tb_product.id_shop = tb_user_shop.id;");

            while (rsSelectProductByShop.next()) {
                Object[] data = {rsSelectProductByShop.getString("id"), rsSelectProductByShop.getString("product_price"),
                    rsSelectProductByShop.getString("product_name"), rsSelectProductByShop.getString("product_stock"), rsSelectProductByShop.getString("shop_name")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public DefaultTableModel SelectProductByCategoryToDataTable(String category) {
        ResultSet rsSelectProductByShop = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"Tampilan Product", "Harga (Rp)", "Nama", "Sisa Barang", "Toko"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT tb_product.*, tb_user_shop.shop_name as shop_name FROM tb_product LEFT JOIN tb_user_shop on tb_product.id_shop = tb_user_shop.id WHERE tb_product.product_category = '" + category + "';");

            while (rsSelectProductByShop.next()) {
                Object[] data = {rsSelectProductByShop.getString("id"), rsSelectProductByShop.getString("product_price"),
                    rsSelectProductByShop.getString("product_name"), rsSelectProductByShop.getString("product_stock"), rsSelectProductByShop.getString("shop_name")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public DefaultTableModel SelectProductByNameToDataTable(String category) {
        ResultSet rsSelectProductByShop = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"Tampilan Product", "Harga (Rp)", "Nama", "Sisa Barang", "Toko"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT tb_product.*, tb_user_shop.shop_name as shop_name FROM tb_product LEFT JOIN tb_user_shop on tb_product.id_shop = tb_user_shop.id WHERE product_name LIKE '%" + category + "%'");

            while (rsSelectProductByShop.next()) {
                Object[] data = {rsSelectProductByShop.getString("id"), rsSelectProductByShop.getString("product_price"),
                    rsSelectProductByShop.getString("product_name"), rsSelectProductByShop.getString("product_stock"), rsSelectProductByShop.getString("shop_name")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public DefaultTableModel SelectTransactionAllToDataTable() {
        ResultSet rsSelectTransaction = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"No Transaksi", "Nama Barang", "Kuantitas Barang", "Harga Barang", "Status"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectTransaction = stm.executeQuery("SELECT * FROM tb_transaction");

            while (rsSelectTransaction.next()) {
                Object[] data = {rsSelectTransaction.getString("id_transaction"), rsSelectTransaction.getString("nama_barang"), rsSelectTransaction.getString("kuantitas_barang"), rsSelectTransaction.getString("harga_barang"), rsSelectTransaction.getString("status")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public ResultSet SelectShopByIdProfile(String idProfile) {
        ResultSet rsSelectShopByIdProfile = null;
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectShopByIdProfile = stm.executeQuery("SELECT * FROM tb_user_shop WHERE id_profile = '" + idProfile + "'");

            rsSelectShopByIdProfile.next();

            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return rsSelectShopByIdProfile;
    }

    public boolean InsertProductByShop(List<imageObject> id_images, String product_price, String product_name, String product_category,
            String product_exp, String product_desc, String product_stock, String product_satuan, String product_address, String product_insurance,
            String product_merk, String product_contact, String id_shop) {
        int rsInsertProductPenjual = 0;
        boolean result = false;
        ArrayList<Product> resultList = new ArrayList<Product>();
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();
//            InputStream is = new FileInputStream(imagePath);

//            ResultSet rsInsertUserImage = stm.executeQuery("INSERT INTO tb_blob_image (image_name, image_path, image_file) VALUES ('" + imageName + "','" + imagePath + "','" + is + "')");
//            ResultSet rsSelectUserImage = stm.executeQuery("SELECT id FROM tb_blob_image ORDER BY id desc");
            rsInsertProductPenjual = stm.executeUpdate("INSERT INTO tb_product(product_price, product_name, product_category, product_exp,"
                    + " product_desc, product_stock, product_satuan, product_address, product_insurance, product_merk, product_contact, id_shop) VALUES ('"
                    + product_price + "', '" + product_name + "', '" + product_category + "', '" + product_exp + "', '"
                    + product_desc + "', '" + product_stock + "', '" + product_satuan + "', '" + product_address + "', '" + product_insurance + "', '" + product_merk
                    + "', '" + product_contact + "', '" + id_shop + "');");

            if (rsInsertProductPenjual == 1) {
                Statement stm2 = conn.createStatement();
                ResultSet rsSelectIdProduct = stm2.executeQuery("SELECT id FROM tb_product ORDER BY id desc");
                rsSelectIdProduct.next();
                for (int i = 0; i < id_images.size(); i++) {
                    InsertImage(id_images.get(i).getImageName(), id_images.get(i).getImagePath(), id_images.get(i).getImageByte(), rsSelectIdProduct.getString("id"), "product");
                }
                result = true;
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return result;
    }

    public boolean InsertTransactionByShop(String namaBarang, String kuantitasBarang, String hargaBarang, String status) {
        boolean result = false;
        ArrayList<Product> resultList = new ArrayList<Product>();
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            int rsInsertTransaction = stm.executeUpdate("INSERT INTO tb_transaction (nama_barang, kuantitas_barang, harga_barang, status) VALUES ('" + namaBarang + "', '" + kuantitasBarang + "', ' " + hargaBarang + " ', '" + status + "')");

            if (rsInsertTransaction == 1) {
                result = true;
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return result;
    }

    public DefaultTableModel SelectArticle(String idProfile) {
        ResultSet rsSelectProductByShop = null;
        ArrayList<Product> resultList = new ArrayList<Product>();
        String col[] = {"id", "title", "writer", "date", "path"};
        DefaultTableModel tableModel = new DefaultTableModel(col, 0);
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();

            rsSelectProductByShop = stm.executeQuery("SELECT * FROM tb_article");

            while (rsSelectProductByShop.next()) {
                Object[] data = {rsSelectProductByShop.getString("id"), rsSelectProductByShop.getString("title"), rsSelectProductByShop.getString("writer"),
                    rsSelectProductByShop.getString("date"), rsSelectProductByShop.getString("path")};

                tableModel.addRow(data);
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return tableModel;
    }

    public boolean InsertArticle(String title, String writer, String date, String path) {
        int rsInsertArticle = 0;
        boolean result = false;
        try {
            Connection conn = openConnection();
            Statement stm = conn.createStatement();
//            InputStream is = new FileInputStream(imagePath);

//            ResultSet rsInsertUserImage = stm.executeQuery("INSERT INTO tb_blob_image (image_name, image_path, image_file) VALUES ('" + imageName + "','" + imagePath + "','" + is + "')");
//            ResultSet rsSelectUserImage = stm.executeQuery("SELECT id FROM tb_blob_image ORDER BY id desc");
            rsInsertArticle = stm.executeUpdate("INSERT INTO maggotdb.tb_product(title, writer, date, path) VALUES ('"
                    + title + "', '" + writer + "', '" + date + "', '" + path + "');");

            if (rsInsertArticle == 1) {
                result = true;
            }
            closeConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Error " + e);
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            System.out.print(dbConnection.class.getName() + " : " + ex.getMessage());
        }
        return result;
    }

}
