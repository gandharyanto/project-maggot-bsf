CREATE TABLE `tb_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `writer` varchar(100) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `path` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_blob_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(200) NOT NULL,
  `image_path` varchar(200) NOT NULL,
  `image_file` longblob NOT NULL,
  `id_relation` varchar(50) NOT NULL,
  `type_relation` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_price` decimal(10,0) DEFAULT NULL,
  `product_name` varchar(45) DEFAULT NULL,
  `product_category` varchar(45) DEFAULT NULL,
  `product_exp` varchar(45) DEFAULT NULL,
  `product_desc` varchar(45) DEFAULT NULL,
  `product_stock` varchar(45) DEFAULT NULL,
  `product_satuan` varchar(45) DEFAULT NULL,
  `product_address` varchar(45) DEFAULT NULL,
  `product_insurance` varchar(45) DEFAULT NULL,
  `product_merk` varchar(45) DEFAULT NULL,
  `product_contact` varchar(45) DEFAULT NULL,
  `id_shop` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_sale` (
  `id_transaction` varchar(20) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_qty` varchar(45) NOT NULL,
  `product_price` int(11) DEFAULT NULL,
  `transaction_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_transaction` (
  `id_transaction` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` varchar(45) DEFAULT NULL,
  `kuantitas_barang` varchar(45) DEFAULT NULL,
  `harga_barang` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_transaction`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `birth_place` varchar(100) DEFAULT NULL,
  `birth_date` varchar(20) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `tb_user_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_profile` int(11) DEFAULT NULL,
  `shop_name` varchar(100) DEFAULT NULL,
  `shop_category` varchar(45) DEFAULT NULL,
  `shop_address` varchar(255) DEFAULT NULL,
  `shop_description` varchar(100) DEFAULT NULL,
  `shop_type` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
